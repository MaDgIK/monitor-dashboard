import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer, Meta, Title} from '@angular/platform-browser';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {StringUtils} from '../openaireLibrary/utils/string-utils.class';
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {IndicatorPath} from "../openaireLibrary/monitor/entities/stakeholder";
import {StatisticsService} from "../openaireLibrary/monitor-admin/utils/services/statistics.service";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {Subscription} from "rxjs";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {IndexInfoService} from "../openaireLibrary/utils/indexInfo.service";
import {RangeFilterComponent} from "../openaireLibrary/utils/rangeFilter/rangeFilter.component";
import {
  MonitorIndicatorStakeholderBaseComponent
} from "../openaireLibrary/monitor/monitor-indicator-stakeholder-base.component";

@Component({
  selector: 'monitor',
  templateUrl: 'monitor.component.html',
  styleUrls: ['monitor.component.less']
})
export class MonitorComponent extends MonitorIndicatorStakeholderBaseComponent implements OnInit {
  @ViewChild('rangeFilter') rangeFilter: RangeFilterComponent;
  privateStakeholder = false;
  public statsUpdateDate: Date;

  constructor(
    protected _route: ActivatedRoute,
    protected _router: Router,
    protected _meta: Meta,
    protected _title: Title,
    protected _piwikService: PiwikService,
    protected seoService: SEOService,
    protected sanitizer: DomSanitizer,
    protected cdr: ChangeDetectorRef,
    protected layoutService: LayoutService,
    protected statisticsService: StatisticsService,
    private stakeholderService: StakeholderService,
    private userManagementService: UserManagementService,
    private indexInfoService: IndexInfoService) {
    super();
  }
  
  public ngOnInit() {
    if (typeof document !== 'undefined') {
      this.subscriptions.push(this.indexInfoService.getStatsLastDate(this.properties).subscribe(lastIndexUpdate => {
        if (lastIndexUpdate) {
          this.statsUpdateDate = new Date(lastIndexUpdate);
        }
      }));
    }
    let subscription: Subscription;

    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      this.subscriptions.push(this._route.params.subscribe(params => {
        this.loading = true;
        this.activeTopic = null;
        this.activeCategory = null;
        this.activeSubCategory = null;
        if (subscription) {
          subscription.unsubscribe();
        }
        if (!this.stakeholder || (params['child'] && this.stakeholder.alias !== params['child']) ||
            (!params['child'] && this.stakeholder.alias !== params['stakeholder'])) {
          this.numberResults = new Map<string, number>();
          subscription = this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
            if (stakeholder) {
              this.stakeholder = stakeholder;
              // add fl0 filter only for EC
              if (this.stakeholder.index_id == "ec__________::EC") {
                this.filters.push({
                  title: "Funding Stream",
                  filterId: "relfundinglevel0_id",
                  originalFilterId: "relfundinglevel0_id",
                  countSelectedValues: 0,
                  values: [{name: "EC|FP7", id: "ec__________::EC::FP7", selected: false, number: 0}, {
                    name: "EC|H2020",
                    id: "ec__________::EC::H2020",
                    selected: false,
                    number: 0
                  }]
                  ,
                  filterOperator: "or",
                  valueIsExact: true,
                  filterType: "radio",
                  radioValue: ""
                });
              }
              if (this.stakeholder.type == "funder") {
                // this.filters.push({title: "Co-funded research outcomes",filterId: "co-funded",originalFilterId: "co-funded", countSelectedValues: 0,
                //   values:[{fname: "true", id: "co-funded", selected:  false, number: 0}, {name: "false", id: "no-co-funded", selected:  false, number: 0}]
                //   ,filterOperator: "or", valueIsExact: true, filterType: "radio", radioValue:""});
                this.filters.push({
                  title: "Co-funded", filterId: "co-funded", originalFilterId: "co-funded", countSelectedValues: 0,
                  values: [{name: "Co-funded research output", id: "co-funded-results", selected: false, number: 0}]
                  , filterOperator: "or", valueIsExact: true, filterType: "checkbox", radioValue: ""
                });
              }
              this.subscriptions.push(this._route.queryParams.subscribe(queryParams => {
                this.handleQueryParams(queryParams, params);
                this.title = this.stakeholder.parent ? "Monitor Dashboard | " + this.stakeholder.parent.name + " | " + this.stakeholder.name : "Monitor Dashboard | " + this.stakeholder.name;
                this.description = this.stakeholder.parent ? "Monitor Dashboard | " + this.stakeholder.parent.name + " | " + this.stakeholder.name : "Monitor Dashboard | " + this.stakeholder.name;
                this.setMetadata();
                if (this.hasPermission((this.view && this.isManager(this.stakeholder))?this.view:this.stakeholder.visibility)) {
                  this.setView(params);
                } else {
                  this.privateStakeholder = true;
                  if (subscription) {
                    subscription.unsubscribe();
                  }
                }
              }));
            } else {
              this.navigateToError();
              if (subscription) {
                subscription.unsubscribe();
              }
            }
          });
          this.subscriptions.push(subscription);
        } else {
          this.trackView();
          this.subscriptions.push(this._route.queryParams.subscribe(queryParams => {
            this.handleQueryParams(queryParams, params);
          }));
        }
      }));
    }));
  }

  private getfl0() {
    if (this.queryParams["relfundinglevel0_id"] && this.filters.length > 0) {
      let value = StringUtils.URIDecode(StringUtils.unquote(this.queryParams["relfundinglevel0_id"]));
      return value.split("::")[value.split("::").length - 1];
    }
    return null;
  }

  private getCoFunded() {
    if (this.queryParams["co-funded"] && this.filters.length > 0) {
      return this.queryParams["co-funded"] && StringUtils.URIDecode(StringUtils.unquote(this.queryParams["co-funded"])) == "co-funded-results";
    }
    return false;
  }

  public getFullUrl(indicatorPath: IndicatorPath) {
    return this.indicatorUtils.getFullUrlWithFilters(this.stakeholder, indicatorPath, this.getfl0(), this.periodFilter.selectedFromValue, this.periodFilter.selectedToValue, this.getCoFunded());
  }
  
  public get feedback() {
    return "mailto:" + this.properties.feedbackmail + "?subject=%5BOpenAIRE%20Monitor%5D%20" + (this.stakeholder ? this.stakeholder.name : "") + "%20dashboard%20feedback"
  }

  logIn() {
    this.userManagementService.login();
  }
}
