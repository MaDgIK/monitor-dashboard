import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {Monitor} from "./monitor";

let routes: Routes = new Monitor(':stakeholder').routes;
routes.splice(0, 0, {path: '', redirectTo: '/admin', pathMatch: 'full'});
routes.splice(2, 0, {
  path: ':stakeholder/browse/:type',
  loadChildren: () => import('../browse-stakeholders/browse-stakeholders.module').then(m => m.BrowseStakeholdersModule),
  canDeactivate: [PreviousRouteRecorder],
  data: {
    activeMenuItem: "dashboard"
  }
});

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class MonitorRoutingModule {
}
