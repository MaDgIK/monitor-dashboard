import {Routes} from "@angular/router";
import {MonitorComponent} from "./monitor.component";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {OpenaireErrorPageComponent} from "../error/errorPage.component";

export class Monitor {
  routes: Routes;

  constructor(param: ':stakeholder' | ':child') {
    this.routes = [
      {
        path: param,
        component: MonitorComponent,
        canDeactivate: [PreviousRouteRecorder],
        data: {
          activeMenuItem: "dashboard"
        }
      },
      {
        path: param + '/indicators',
        loadChildren: () => import('../openaireLibrary/monitor/indicators/indicators.module').then(m => m.IndicatorsModule),
        canDeactivate: [PreviousRouteRecorder],
        data: {
          hasSidebar: false
        }
      },
      {
        path: param + '/develop',
        loadChildren: () => import('../develop/develop.module').then(m => m.DevelopModule),
        canDeactivate: [PreviousRouteRecorder],
        data: {
          hasSidebar: false
        }
      },
      {
        path: param + '/methodology',
        loadChildren: () => import('../openaireLibrary/monitor/methodology/methodology.module').then(m => m.MethodologyModule),
        canDeactivate: [PreviousRouteRecorder],
        data: {
          hasSidebar: false
        }
      },
      {
        path: param + '/search',
        loadChildren: () => import('../search/search.module').then(m => m.SearchModule),
        canDeactivate: [PreviousRouteRecorder],
        data: {
          hasSidebar: false,
          activeMenuItem: param === ':stakeholder'?"search":'dashboard'
        }
      },
      {
        path: param + '/error',
        component: OpenaireErrorPageComponent,
        data: {hasSidebar: false}
      },
      {
        path: param + '/user-info',
        loadChildren: () => import('../login/libUser.module').then(m => m.LibUserModule),
        data: {hasSidebar: false}
      },
      {
        path: param + '/:topic',
        component: MonitorComponent,
        canDeactivate: [PreviousRouteRecorder],
        data: {
          activeMenuItem: "dashboard"
        }
      },
      {
        path: param + '/:topic/:category',
        component: MonitorComponent,
        canDeactivate: [PreviousRouteRecorder],
        data: {
          activeMenuItem: "dashboard"
        }
      },
      {
        path: param + '/:topic/:category/:subCategory',
        component: MonitorComponent,
        canDeactivate: [PreviousRouteRecorder],
        data: {
          activeMenuItem: "dashboard"
        }
      }
    ]
  }
}
