import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {ErrorMessagesModule} from '../openaireLibrary/utils/errorMessages.module';
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {MonitorRoutingModule} from "./monitor-routing.module";
import {MonitorComponent} from "./monitor.component";
import {StatisticsService} from "../openaireLibrary/monitor-admin/utils/services/statistics.service";
import {SideBarModule} from "../openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {UserMiniModule} from "../openaireLibrary/login/userMiniModule.module";
import {ClickModule} from "../openaireLibrary/utils/click/click.module";
import {BottomModule} from "../openaireLibrary/sharedComponents/bottom.module";
import {RangeFilterModule} from "../openaireLibrary/utils/rangeFilter/rangeFilter.module";
import {SearchFilterModule} from "../openaireLibrary/searchPages/searchUtils/searchFilter.module";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {filters, incognito} from "../openaireLibrary/utils/icons/icons";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {NumberRoundModule} from "../openaireLibrary/utils/pipes/number-round.module";
import {SliderTabsModule} from "../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {SliderUtilsModule} from "../openaireLibrary/sharedComponents/slider-utils/slider-utils.module";
import {
  SidebarMobileToggleModule
} from "../openaireLibrary/dashboard/sharedComponents/sidebar/sidebar-mobile-toggle/sidebar-mobile-toggle.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, ErrorMessagesModule,
    HelperModule, Schema2jsonldModule, SEOServiceModule, MonitorRoutingModule, SideBarModule, InputModule,
    UserMiniModule, ClickModule, BottomModule, RangeFilterModule, SearchFilterModule, PageContentModule, IconsModule,
    LogoUrlPipeModule, NumberRoundModule, SliderTabsModule, SliderUtilsModule, SidebarMobileToggleModule
  ],
  declarations: [
    MonitorComponent
  ],
  providers: [
    PreviousRouteRecorder,
    PiwikService,
    StatisticsService
  ],
  exports: [
    MonitorComponent
  ]
})
export class MonitorModule {
	constructor(private iconsService: IconsService) {
		this.iconsService.registerIcons([incognito, filters]);
	}
}
