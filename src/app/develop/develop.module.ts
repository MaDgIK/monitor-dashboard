import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DevelopComponent} from "./develop.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";

@NgModule({
  declarations: [DevelopComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: DevelopComponent,
      canDeactivate: [PreviousRouteRecorder]
    },
  ]), PageContentModule, IconsModule],
  exports: [DevelopComponent]
})
export class DevelopModule {

}
