import {Component, OnDestroy, OnInit} from "@angular/core";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {Stakeholder} from "../openaireLibrary/monitor/entities/stakeholder";
import {Subscription} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {properties} from "../../environments/environment";
import {Router} from "@angular/router";
import {StakeholderUtils} from "../openaireLibrary/monitor-admin/utils/indicator-utils";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";

@Component({
  selector: 'develop',
  template: `
      <div class="uk-section">
				<div class="uk-container uk-container-large">
						<h1>Help developers <br> with OpenAIRE APIs<span class="uk-text-primary">.</span></h1>
				</div>
				<div class="uk-section uk-container uk-container-large">
					<div class="uk-grid uk-grid-large uk-child-width-1-3@m uk-child-width-1-1" uk-grid>
						<div class="uk-text-center uk-margin-large-top">
							<div class="uk-icon-bg-shadow uk-icon-bg-shadow-large uk-margin-auto">
								<icon name="description" customClass="uk-text-background" [flex]="true" ratio="2.5" type="outlined" visuallyHidden="For {{openaireEntities.RESULTS}}"></icon>
							</div>
							<h3>For {{openaireEntities.RESULTS | lowercase}}</h3>
							<div class="uk-margin-bottom">
								For {{openaireEntities.RESULTS | lowercase}} ({{openaireEntities.PUBLICATIONS | lowercase}}, {{openaireEntities.DATASETS | lowercase}}, {{openaireEntities.SOFTWARE | lowercase}} and {{openaireEntities.OTHER | lowercase}}) you can use the Graph API by adding the funder parameter.
							</div>
							<a class="uk-display-inline-block uk-button uk-button-text"
									href="https://graph.openaire.eu/docs/apis/search-api/research-products" target="_blank">
								<span class="uk-flex uk-flex-middle">
									<span>Graph API</span>
								</span>
							</a>
						</div>
						<div>
							<img src="assets/apis.svg" alt="">
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<div class="uk-icon-bg-shadow uk-icon-bg-shadow-large uk-margin-auto">
								<icon name="content_paste" customClass="uk-text-background" [flex]="true" ratio="2.5" type="outlined" visuallyHidden="For {{openaireEntities.PROJECTS}}"></icon>
							</div>
							<h3>For {{openaireEntities.PROJECTS | lowercase}}</h3>
							<div class="uk-margin-bottom">
								For {{openaireEntities.PROJECTS | lowercase}} you can use the Graph API and the Bulk Access APIs.
							</div>
							<div class="uk-flex uk-flex-column uk-flex-center uk-flex-middle">
								<a class="uk-display-inline-block uk-button uk-button-text uk-margin-bottom"
									href="https://graph.openaire.eu/docs/apis/search-api/projects" target="_blank">
									<span class="uk-flex uk-flex-middle">
										<span>Graph API</span>
									</span>
								</a>
								<a class="uk-display-inline-block uk-button uk-button-text"
									href="https://graph.openaire.eu/docs/apis/dspace-eprints-api" target="_blank">
									<span class="uk-flex uk-flex-middle">
										<span>Bulk Access APIs</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-section uk-container uk-container-large">
					<div class="uk-grid uk-grid-large uk-child-width-1-2@m uk-child-width-1-1" uk-grid>
						<div>
							<div>Request examples for {{openaireEntities.RESULTS | lowercase}}:</div>
							<ul class="uk-list uk-list-large uk-list-bullet uk-list-primary">
                <li>
                  <span>Access all </span><span class="uk-text-bolder">{{openaireEntities.RESULTS}}</span> ({{openaireEntities.PUBLICATIONS}}, {{openaireEntities.DATASETS}}, {{openaireEntities.SOFTWARE}}, {{openaireEntities.OTHER}})<br>
                  <span class="uk-text-bold uk-margin-small-right">GET</span>
                  <span class="">https://api.openaire.eu/search/researchProducts?funder={{stakeholder.index_shortName}}</span>
                </li>
                <li>
                  <span>Access </span><span class="uk-text-bolder">{{openaireEntities.PUBLICATIONS}}</span><br>
                  <span class="uk-text-bold uk-margin-small-right">GET</span>
                  <span class="">https://api.openaire.eu/search/publications?funder={{stakeholder.index_shortName}}</span>
                </li>
								<li>
                  <span>Access </span><span class="uk-text-bolder">Open Access {{openaireEntities.PUBLICATIONS}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">http://api.openaire.eu/search/publications?funder={{stakeholder.index_shortName}}&OA=true</span>
								</li>
								<li>
                  <span>Access </span><span class="uk-text-bolder">{{openaireEntities.DATASETS}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/datasets?funder={{stakeholder.index_shortName}}</span>
								</li>
								<li>
                  <span>Access </span><span class="uk-text-bolder">{{openaireEntities.SOFTWARE}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/software?funder={{stakeholder.index_shortName}}</span>
								</li>
								<li>
                  <span>Access </span><span class="uk-text-bolder">{{openaireEntities.OTHER}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/other?funder={{stakeholder.index_shortName}}</span>
								</li>
							</ul>
						</div>
						<div>
							<div>Request examples for {{openaireEntities.PROJECTS | lowercase}}:</div>
							<ul class="uk-list uk-list-large uk-list-bullet uk-list-primary">
							<li>
                <span>For the </span><span class="uk-text-bolder">Selective Access</span><br>
								<span class="uk-text-break">https://api.openaire.eu/search/projects?funder={{stakeholder.index_shortName}}</span>
							</li>
							<li>
                <span>For the </span><span class="uk-text-bolder">Bulk Access</span><br>
								<span class="uk-text-bold uk-text-nowrap">DSpace endpoint:</span>
								<span class="uk-text-break uk-margin-small-left">https://api.openaire.eu/projects/dspace/{{stakeholder.index_shortName}}/ALL/ALL</span><br>
								<span class="uk-text-bold uk-text-nowrap">ePrints endpoint:</span>
								<span class="uk-text-break uk-margin-small-left">https://api.openaire.eu/projects/eprints/{{stakeholder.index_shortName}}/ALL/ALL</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
  `
})
export class DevelopComponent implements OnInit, OnDestroy {
  
  public stakeholder: Stakeholder;
  private subscriptions: any[] = [];
  private stakeholderUtils: StakeholderUtils = new StakeholderUtils();
  public type: string;
  public openaireEntities = OpenaireEntities;
  public properties: EnvProperties = properties;
  
  constructor(private stakeholderService: StakeholderService,
              private seoService: SEOService,
              private _meta: Meta,
              private piwikService: PiwikService,
              private _router: Router,
              private _title: Title) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
      this.stakeholder = stakeholder;
      if (this.stakeholder) {
        if(stakeholder.type !== "funder") {
          this.navigateToError();
        }
        /* Metadata */
        const url = properties.domain + properties.baseLink + this._router.url;
        this.seoService.createLinkForCanonicalURL(url, false);
        this._meta.updateTag({content: url}, "property='og:url'");
        const description = "Develop | " + this.stakeholder.name;
        const title = "Develop | " + this.stakeholder.name;
        this._meta.updateTag({content: description}, "name='description'");
        this._meta.updateTag({content: description}, "property='og:description'");
        this._meta.updateTag({content: title}, "property='og:title'");
        this._title.setTitle(title);
        this.subscriptions.push(this.piwikService.trackView(properties, title).subscribe());
        /* Initializations */
        this.stakeholderUtils.types.forEach(type => {
          if (type.value === stakeholder.type) {
            this.type = type.label;
          }
        });
      }
    }));
  }
  
  private navigateToError() {
    this._router.navigate([this.properties.errorLink], {queryParams: {'page': this._router.url}});
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
