import {properties} from "../../environments/environment";

interface Links {
  searchLinkToResult;
  searchLinkToPublication;
  searchLinkToProject;
  searchLinkToDataProvider;
  searchLinkToDataset;
  searchLinkToSoftwareLanding;
  searchLinkToOrp;
  searchLinkToOrganization;
  searchLinkToResults,
  searchLinkToPublications,
  searchLinkToDatasets,
  searchLinkToSoftware,
  searchLinkToOrps,
  searchLinkToProjects,
  searchLinkToDataProviders,
  searchLinkToOrganizations,
  searchLinkToAdvancedResults,
  searchLinkToAdvancedPublications,
  searchLinkToAdvancedDatasets
  searchLinkToAdvancedSoftware,
  searchLinkToAdvancedOrps,
  searchLinkToAdvancedProjects,
  searchLinkToAdvancedDataProviders,
  searchLinkToAdvancedOrganizations,
  errorLink
}

export class LinksResolver {
  
  public static default: Links = {
    searchLinkToResult: properties.searchLinkToResult,
    searchLinkToPublication: properties.searchLinkToPublication,
    searchLinkToProject: properties.searchLinkToProject,
    searchLinkToDataProvider: properties.searchLinkToDataProvider,
    searchLinkToDataset: properties.searchLinkToDataset,
    searchLinkToSoftwareLanding: properties.searchLinkToSoftwareLanding,
    searchLinkToOrp: properties.searchLinkToOrp,
    searchLinkToOrganization: properties.searchLinkToOrganization,
    searchLinkToResults: properties.searchLinkToResults,
    searchLinkToPublications: properties.searchLinkToPublications,
    searchLinkToDatasets: properties.searchLinkToDatasets,
    searchLinkToSoftware: properties.searchLinkToSoftware,
    searchLinkToOrps: properties.searchLinkToOrps,
    searchLinkToDataProviders: properties.searchLinkToDataProviders,
    searchLinkToProjects: properties.searchLinkToProjects,
    searchLinkToOrganizations: properties.searchLinkToOrganizations,
    searchLinkToAdvancedResults: properties.searchLinkToAdvancedResults,
    searchLinkToAdvancedPublications: properties.searchLinkToAdvancedPublications,
    searchLinkToAdvancedDatasets: properties.searchLinkToAdvancedDatasets,
    searchLinkToAdvancedSoftware: properties.searchLinkToAdvancedSoftware,
    searchLinkToAdvancedOrps: properties.searchLinkToAdvancedOrps,
    searchLinkToAdvancedProjects: properties.searchLinkToAdvancedProjects,
    searchLinkToAdvancedDataProviders: properties.searchLinkToAdvancedDataProviders,
    searchLinkToAdvancedOrganizations: properties.searchLinkToAdvancedOrganizations,
    errorLink: properties.errorLink
  };
  
  public static setProperties(alias: string) {
    Object.keys(this.default).forEach(field => {
      properties[field] = "/" + alias + (<string>this.default[field]);
    });
  }
  
  public static resetProperties() {
    Object.keys(this.default).forEach(field => {
      properties[field] = this.default[field];
    });
  }
}
