import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorSoftwareComponent} from './software.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: MonitorSoftwareComponent, canDeactivate: [PreviousRouteRecorder]  }
  ])
]
})
export class SoftwareRoutingModule { }
