import { NgModule}            from '@angular/core';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import { MonitorSoftwareComponent } from './software.component';
import {SoftwareRoutingModule} from './software-routing.module';
import {ResultLandingModule} from "../../../openaireLibrary/landingPages/result/resultLanding.module";
import {CommonModule} from "@angular/common";
@NgModule({
  imports: [SoftwareRoutingModule, ResultLandingModule, CommonModule],
  declarations:[MonitorSoftwareComponent],
  providers:[ PreviousRouteRecorder],
  exports:[MonitorSoftwareComponent]
})
export class LibSoftwareModule { }
