import { NgModule}            from '@angular/core';
import { DataProviderModule } from '../../../openaireLibrary/landingPages/dataProvider/dataProvider.module';
import { MonitorDataProviderComponent } from './dataProvider.component';
import {DataProviderRoutingModule} from './dataProvider-routing.module';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [DataProviderModule, DataProviderRoutingModule, CommonModule],
  declarations:[MonitorDataProviderComponent],
  providers:[ PreviousRouteRecorder],
  exports:[MonitorDataProviderComponent]
})
export class LibDataProviderModule { }
