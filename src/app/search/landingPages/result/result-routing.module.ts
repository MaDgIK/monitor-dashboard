import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorResultComponent} from './result.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: MonitorResultComponent, canDeactivate: [PreviousRouteRecorder]  }
  ])
]
})
export class ResultRoutingModule { }
