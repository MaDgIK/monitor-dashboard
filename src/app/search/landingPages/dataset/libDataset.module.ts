import {NgModule} from '@angular/core';
import {MonitorDatasetComponent} from './dataset.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {DatasetRoutingModule} from './dataset-routing.module';
import {ResultLandingModule} from "../../../openaireLibrary/landingPages/result/resultLanding.module";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [DatasetRoutingModule, ResultLandingModule, CommonModule],
  declarations:[MonitorDatasetComponent],
  providers:[PreviousRouteRecorder],
  exports:[MonitorDatasetComponent]
})
export class LibDatasetModule { }
