import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorDatasetComponent} from './dataset.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: MonitorDatasetComponent, canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class DatasetRoutingModule {
}
