import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorOrpComponent} from './orp.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([{
      path: '', component: MonitorOrpComponent, canDeactivate: [PreviousRouteRecorder]
    }])
  ]
})

export class OrpRoutingModule { }
