import {Component, Input} from '@angular/core';
import {SearchCustomFilter} from "../../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'monitor-search-projects',
  template: `
      <search-projects *ngIf="initialized"
                       [customFilter]=customFilter [hasPrefix]="false"
                       [includeOnlyResultsAndFilter]="false" 
                       [openaireLink]="'https://'+(properties.environment != 'production'?'beta.':'')+'explore.openaire.eu/search/simple/projects'" 
                       [searchForm]="searchForm">
      </search-projects>
  `
  
})
export class MonitorSearchProjectsComponent {
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  customFilter: SearchCustomFilter = null;
  initialized: boolean = false;
  properties;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }
  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  ngOnInit() {
    this.properties = properties;
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            if (stakeholder.type === "funder") {
              let value = stakeholder.index_id+"||"+stakeholder.index_name+"||"+stakeholder.index_shortName;
              this.customFilter = new SearchCustomFilter("Funder", "funder", value, "");
            } else if (stakeholder.type === "organization") {
              let value = stakeholder.index_id;
              this.customFilter = new SearchCustomFilter("Organization", "relorganizationid", value, "");
            } else if (stakeholder.type === "ri") {
              this.navigateToError();
            }
            this.initialized = true;
          }
        }));
      }
    }));
  }
  
  navigateToError() {
    this.router.navigate([this.properties.errorLink], {queryParams: {'page': this.router.url}});
  }
}
