import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{MonitorSearchDataprovidersComponent} from './searchDataproviders.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorSearchDataprovidersComponent, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class SearchDataProvidersRoutingModule { }
