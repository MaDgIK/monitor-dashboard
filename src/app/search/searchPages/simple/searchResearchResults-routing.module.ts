import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorSearchResearchResultsComponent} from './searchResearchResults.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorSearchResearchResultsComponent, canDeactivate: [PreviousRouteRecorder]  }
    ])
  ]
})
export class SearchResearchResultsRoutingModule { }
