import {Component, Input} from '@angular/core';
import {SearchCustomFilter} from "../../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";
import {ConfigurationService} from "../../../openaireLibrary/utils/configuration/configuration.service";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";
import {EnvProperties} from "../../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../../environments/environment";

@Component({
  selector: 'monitor-search-dataproviders',
  template: `
    <search-dataproviders *ngIf="initialized"
                          [customFilter]=customFilter [hasPrefix]="false" [searchForm]="searchForm"
                          [includeOnlyResultsAndFilter]="false" [showSwitchSearchLink]="showSwitchSearchLink">
    </search-dataproviders>
  `
})
export class MonitorSearchDataprovidersComponent {
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  customFilter: SearchCustomFilter = null;
  initialized: boolean = false;
  showSwitchSearchLink: boolean = false;
  public properties: EnvProperties = properties;
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }
  
  subscriptions = [];
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            if (stakeholder.type === "funder") {
              this.navigateToError();
            } else if (stakeholder.type === "organization") {
              let value = stakeholder.index_id;
              this.customFilter = new SearchCustomFilter("Organization", "relorganizationid", value, "");
            } else if (stakeholder.type === "ri") {
              this.navigateToError();
            }
            this.initialized = true;
          }
        }));
      }
    }));
  }
  
  navigateToError() {
    this.router.navigate([this.properties.errorLink], {queryParams: {'page': this.router.url}});
  }
}
