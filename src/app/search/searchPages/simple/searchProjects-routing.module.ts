import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorSearchProjectsComponent} from './searchProjects.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorSearchProjectsComponent, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class SearchProjectsRoutingModule { }
