import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";
import {ConfigurationService} from "../../../openaireLibrary/utils/configuration/configuration.service";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'monitor-search-organizations',
  template: `
      <search-organizations *ngIf="initialized" [searchForm]="searchForm">
      </search-organizations>
  `
})
export class MonitorSearchOrganizationsComponent {
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  initialized: boolean = false;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService,  private configurationService: ConfigurationService) {
  }
  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            this.initialized = true;
          }
        }));
      }
    }));
  }
}
