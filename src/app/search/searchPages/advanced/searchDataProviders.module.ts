import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MonitorAdvancedSearchDataprovidersComponent} from './searchDataproviders.component';
import {SearchDataProvidersRoutingModule} from './searchDataProviders-routing.module';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchDataProvidersModule} from '../../../openaireLibrary/searchPages/searchDataProviders.module';

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchDataProvidersModule, SearchDataProvidersRoutingModule
  
  ],
  declarations: [
    MonitorAdvancedSearchDataprovidersComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    MonitorAdvancedSearchDataprovidersComponent
  ]
})
export class MonitorAdvancedSearchDataProvidersModule {
}
