import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MonitorAdvancedSearchResearchResultsComponent,} from './searchResearchResults.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorAdvancedSearchResearchResultsComponent, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class SearchResearchResultsRoutingModule { }
