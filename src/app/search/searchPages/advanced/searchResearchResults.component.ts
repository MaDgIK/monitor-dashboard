import {Component, Input} from '@angular/core';
import {SearchCustomFilter} from "../../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {ConfigurationService} from "../../../openaireLibrary/utils/configuration/configuration.service";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'monitor-advanced-search-results',
  template: `
      <search-research-results *ngIf="initialized" resultType="result" [simpleView]="false"
                               [customFilter]=customFilter [hasPrefix]="false"
                               [includeOnlyResultsAndFilter]="false"
                               [showSwitchSearchLink]="true"
                               [openaireLink]="'https://'+(properties.environment != 'production'?'beta.':'')+'explore.openaire.eu/search/advanced/research-outcomes'"
                               [searchForm]="searchForm"
      ></search-research-results>
  `
})
export class MonitorAdvancedSearchResearchResultsComponent {
  customFilter: SearchCustomFilter = null;
  initialized: boolean = false;
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  properties;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }
  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  ngOnInit() {
    this.properties = properties;
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            if (stakeholder.type === "funder") {
              let value = stakeholder.index_id+"||"+stakeholder.index_name+"||"+stakeholder.index_shortName;
              this.customFilter = new SearchCustomFilter("Funder", "relfunder", value,  stakeholder.name);
            } else if (stakeholder.type === "organization") {
              let value = stakeholder.index_id;
              this.customFilter = new SearchCustomFilter("Organization", "relorganizationid", value, stakeholder.name);
            } else if (stakeholder.type === "ri") {
              let value =  stakeholder.index_id+"||"+stakeholder.index_name;
              this.customFilter = new SearchCustomFilter("Community", "community", value,  stakeholder.name);
            }
            this.initialized = true;
          }
        }));
      }
    }));
  }
}
