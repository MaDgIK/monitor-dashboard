import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {SearchCustomFilter} from "../../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {Subscriber} from "rxjs";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'monitor-advanced-search-organizations',
  template: `
      <search-organizations *ngIf="initialized" [simpleView]="false" [showSwitchSearchLink]="false" [customFilter]="customFilter" [searchForm]="searchForm">
      </search-organizations>
  `
})
export class MonitorAdvancedSearchOrganizationsComponent {
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  initialized: boolean = false;
  customFilter: SearchCustomFilter = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }
  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            if (stakeholder.type === "funder") {
              let value = stakeholder.index_id+"||"+stakeholder.index_name+"||"+stakeholder.index_shortName;
              this.customFilter = new SearchCustomFilter("Funder", "relfunder", value,  stakeholder.name);
              this.customFilter.isHiddenFilter = false;
            } else if (stakeholder.type === "organization") {
              let value = stakeholder.index_id;
              this.customFilter = new SearchCustomFilter("Organization", "relorganizationid", value, stakeholder.name);
              this.customFilter.isHiddenFilter = false;
            } else if (stakeholder.type === "ri") {
              let value =  stakeholder.index_id+"||"+stakeholder.index_name;
              this.customFilter = new SearchCustomFilter("Community", "community", value,  stakeholder.name);
              this.customFilter.isHiddenFilter = false;
            }
            this.initialized = true;
          }
        }));
      }
    }));
  }
}
