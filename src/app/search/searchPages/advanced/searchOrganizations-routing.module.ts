import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{MonitorAdvancedSearchOrganizationsComponent} from './searchOrganizations.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';



@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorAdvancedSearchOrganizationsComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class SearchOrganizationsRoutingModule { }
