import {Component, Input} from '@angular/core';
import {SearchCustomFilter} from "../../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ActivatedRoute, Router} from "@angular/router";
import {StakeholderService} from "../../../openaireLibrary/monitor/services/stakeholder.service";
import {Subscriber} from "rxjs";
import {SearchForm} from "../../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'monitor-advanced-search-dataproviders',
  template: `
      <search-dataproviders *ngIf="initialized" [simpleView]="false"
                            [customFilter]=customFilter [hasPrefix]="false" [searchForm]="searchForm"
                            [includeOnlyResultsAndFilter]="false" [showSwitchSearchLink]="showSwitchSearchLink">
      </search-dataproviders>
  `
})
export class MonitorAdvancedSearchDataprovidersComponent {
  @Input() searchForm: SearchForm = {class: 'search-form', dark: false};
  customFilter: SearchCustomFilter = null;
  initialized: boolean = false;
  showSwitchSearchLink: boolean = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private stakeholderService: StakeholderService) {
  }

  subscriptions = [];

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (params['stakeholder']) {
        this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
          if (stakeholder) {
            if (stakeholder.type === "organization") {
              let value = stakeholder.index_id;
              this.customFilter = new SearchCustomFilter("Organization", "relorganizationid", value, "");
              this.showSwitchSearchLink = true;
            } else if (stakeholder.type === "funder") {
              let value = stakeholder.index_id + "||" + stakeholder.index_name + "||" + stakeholder.index_shortName;
              this.customFilter = new SearchCustomFilter("Funder", "relfunder", value, stakeholder.name);
            } else if (stakeholder.type === "ri") {
              let value = stakeholder.index_id + "||" + stakeholder.index_name;
              this.customFilter = new SearchCustomFilter("Community", "community", value, stakeholder.name);
            }
            this.initialized = true;
          }
        }));
      }
    }));
  }

}
