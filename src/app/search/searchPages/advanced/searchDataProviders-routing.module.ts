import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{MonitorAdvancedSearchDataprovidersComponent} from './searchDataproviders.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: MonitorAdvancedSearchDataprovidersComponent, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class SearchDataProvidersRoutingModule { }
