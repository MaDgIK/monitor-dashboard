import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    // Search Pages
    { path: '', redirectTo: 'find/research-outcomes', pathMatch: 'full'},
    { path: 'find/research-outcomes', loadChildren:  () => import('./searchPages/simple/searchResearchResults.module').then(m => m.MonitorSearchResearchResultsModule)},
    { path: 'find/projects', loadChildren: () => import('./searchPages/simple/searchProjects.module').then(m => m.MonitorSearchProjectsModule)},
    { path: 'find/dataproviders', loadChildren: () => import('./searchPages/simple/searchDataProviders.module').then(m => m.MonitorSearchDataProvidersModule)},
    { path: 'find/organizations', loadChildren: () => import('./searchPages/simple/searchOrganizations.module').then(m => m.MonitorSearchOrganizationsModule)},
    // Advanced Search Pages
    { path: 'advanced/research-outcomes', loadChildren:  () => import('./searchPages/advanced/searchResearchResults.module').then(m => m.MonitorAdvancedSearchResearchResultsModule)},
    { path: 'advanced/projects', loadChildren: () => import('./searchPages/advanced/searchProjects.module').then(m => m.MonitorAdvancedSearchProjectsModule)},
    { path: 'advanced/dataproviders', loadChildren: () => import('./searchPages/advanced/searchDataProviders.module').then(m => m.MonitorAdvancedSearchDataProvidersModule)},
    { path: 'advanced/organizations', loadChildren: () => import('./searchPages/advanced/searchOrganizations.module').then(m => m.MonitorAdvancedSearchOrganizationsModule)},
    // Landing Pages
    { path: 'result', loadChildren: () => import('./landingPages/result/libResult.module').then(m => m.LibResultModule), data: {hasMenuSearchBar: true}},
    { path: 'publication', loadChildren: () => import('./landingPages/publication/libPublication.module').then(m => m.LibPublicationModule), data: {hasMenuSearchBar: true}},
    { path: 'dataset', loadChildren: () => import('./landingPages/dataset/libDataset.module').then(m => m.LibDatasetModule), data: {hasMenuSearchBar: true}},
    { path: 'software', loadChildren: () => import('./landingPages/software/libSoftware.module').then(m => m.LibSoftwareModule), data: {hasMenuSearchBar: true}},
    { path: 'other', loadChildren: () => import('./landingPages/orp/libOrp.module').then(m => m.LibOrpModule), data: {hasMenuSearchBar: true}},
    { path: 'project', loadChildren: () => import('./landingPages/project/libProject.module').then(m => m.LibProjectModule), data: {hasMenuSearchBar: true}},
    { path: 'dataprovider', loadChildren: () => import('./landingPages/dataProvider/libDataProvider.module').then(m => m.LibDataProviderModule), data: {hasMenuSearchBar: true}},
    { path: 'organization', loadChildren: () => import('./landingPages/organization/libOrganization.module').then(m => m.LibOrganizationModule), data: {hasMenuSearchBar: true}},
  ])]
})
export class SearchModule {}
