import {Component, Input, OnInit} from "@angular/core";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {properties} from "../../environments/environment";
import {Subscriber} from "rxjs";
import {Title} from "@angular/platform-browser";
import {Composer} from "../openaireLibrary/utils/email/composer";
import {Email} from "../openaireLibrary/utils/email/email";
import {ActivatedRoute} from "@angular/router";
import {NotificationUtils} from "../openaireLibrary/notifications/notification-utils";
import {Notification} from "../openaireLibrary/notifications/notifications";
import {User} from "../openaireLibrary/login/utils/helper.class";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {Stakeholder} from "../openaireLibrary/monitor/entities/stakeholder";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";

type Tab = 'member' | 'manager';

@Component({
  selector: 'users',
  templateUrl: 'users.component.html'
})
export class UsersComponent implements OnInit {
 
	public stakeholder: Stakeholder;
  public link: string;
  public loading: boolean;
  public messages: Map<Tab, string> = new Map<Tab, string>();
  public tab: Tab = 'manager';
  public user: User;
  public properties: EnvProperties = properties;
  private subscriptions = [];
  public emailComposer: Function = (name, recipient, role): Email => {
    return Composer.composeEmailForMonitorDashboard(name, recipient, role);
  }
  public notificationFn: Function = (name, recipient, role, invitation): Notification => {
    if(invitation) {
      invitation.link = this.link + invitation.link;
    }
    let notification: Notification = NotificationUtils.invite(this.stakeholder.name, role, 'user', invitation);
    notification.entity = recipient;
    notification.stakeholderType = this.stakeholder.type;
    notification.stakeholder = this.stakeholder.alias;
    notification.name = this.user.firstname;
    notification.surname = this.user.lastname;
    notification.groups = [recipient];
    return notification;
  }
  
  constructor(private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              private route: ActivatedRoute,
              private title: Title) {
  }
  
  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.route.params.subscribe(params => {
      if (this.isTab(params['user_type'])) {
        this.tab = params['user_type'];
      } else {
        this.tab = 'manager';
      }
      if(this.stakeholder) {
        this.title.setTitle(this.stakeholder.name + " | " + this.users);
      }
    }));
    this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
      if (stakeholder) {
				this.stakeholder = stakeholder;
        this.title.setTitle(this.stakeholder.name + " | " + this.users);
        this.link = this.getURL(this.stakeholder.alias);
        this.messages.set("member", 'A member has the right to view the <b>restricted access</b> areas of this indicator\'s profile. ' +
          'A member has <b>no access</b> to the administration part of the profile.');
        this.messages.set("manager", 'A manager has the right to access the administration part of this indicator\'s profile, ' +
          'where he is able to customize and manage indicators, and invite other users as members.');
        this.loading = false;
      }
    }));
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }
  
  get users(): string {
    return StringUtils.capitalize(this.tab) + 's';
  }
  
  private isTab(tab: Tab): boolean {
   switch (tab) {
     case "manager":
       return true;
     case "member":
       return true;
     default:
       return false;
   }
  }
  
  private getURL(id: string): string {
    return properties.domain + properties.baseLink + "/" + id + "?verify=";
  }
}
