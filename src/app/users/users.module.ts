import {NgModule} from "@angular/core";
import {UsersComponent} from "./users.component";
import {CommonModule} from "@angular/common";
import {UsersRoutingModule} from "./users-routing.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {RoleUsersModule} from "../openaireLibrary/dashboard/users/role-users/role-users.module";
import {RouterModule} from "@angular/router";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";

@NgModule({
  imports: [CommonModule, UsersRoutingModule, LoadingModule, PageContentModule, RoleUsersModule, RouterModule, LogoUrlPipeModule],
  declarations: [UsersComponent],
  exports: [UsersComponent]
})
export class UsersModule {
}
