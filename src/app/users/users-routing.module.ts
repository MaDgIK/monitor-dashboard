import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {UsersComponent} from "./users.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: UsersComponent,
        canDeactivate: [PreviousRouteRecorder]
      }
    ])
  ]
})
export class UsersRoutingModule {
}
