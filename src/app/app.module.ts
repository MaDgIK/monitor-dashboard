import {APP_ID, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {OpenaireErrorPageComponent} from './error/errorPage.component';

import {AppRoutingModule} from './app-routing.module';
import {CookieLawModule} from './openaireLibrary/sharedComponents/cookie-law/cookie-law.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {SideBarModule} from "./openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {SharedModule} from "./openaireLibrary/shared/shared.module";
import {Schema2jsonldModule} from "./openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {RoleVerificationModule} from "./openaireLibrary/role-verification/role-verification.module";
import {LoadingModule} from "./openaireLibrary/utils/loading/loading.module";
import {HttpInterceptorService} from "./openaireLibrary/http-interceptor.service";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {AdminLoginGuard} from "./openaireLibrary/login/adminLoginGuard.guard";
import {
  NotificationsSidebarModule
} from "./openaireLibrary/notifications/notifications-sidebar/notifications-sidebar.module";
import {LoginGuard} from "./openaireLibrary/login/loginGuard.guard";
import {IconsModule} from "./openaireLibrary/utils/icons/icons.module";
import {IconsService} from "./openaireLibrary/utils/icons/icons.service";
import {incognito, link} from "./openaireLibrary/utils/icons/icons";
import {AdminDashboardGuard} from "./openaireLibrary/monitor-admin/utils/adminDashboard.guard";
import {CacheIndicatorsModule} from "./openaireLibrary/monitor-admin/utils/cache-indicators/cache-indicators.module";
import {CustomRouteReuseStrategy} from "./openaireLibrary/shared/custom-route-reuse-strategy";
import {RouteReuseStrategy} from "@angular/router";
import {LogoUrlPipeModule} from './openaireLibrary/utils/pipes/logoUrlPipe.module';
import {BreadcrumbsModule} from './openaireLibrary/utils/breadcrumbs/breadcrumbs.module';
import {HelperModule} from "./openaireLibrary/utils/helper/helper.module";

@NgModule({

  imports: [
    SharedModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    ErrorModule,
    FormsModule,
    NavigationBarModule,
    BottomModule,
    CookieLawModule,
    BrowserModule,
    AppRoutingModule,
    SideBarModule, Schema2jsonldModule, RoleVerificationModule, LoadingModule, NotificationsSidebarModule, IconsModule, CacheIndicatorsModule,
    LogoUrlPipeModule,
    BreadcrumbsModule, HelperModule
  ],
  declarations: [AppComponent, OpenaireErrorPageComponent],
  exports: [AppComponent],
  providers: [
    {provide: RouteReuseStrategy, useClass: CustomRouteReuseStrategy},
    {provide: APP_ID, useValue: 'monitor-dashboard'},
    AdminLoginGuard, AdminDashboardGuard, LoginGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 30000}]
  ],
  bootstrap: [AppComponent]
})
//
export class AppModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([incognito, link])
  }
}
