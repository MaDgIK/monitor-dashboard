import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OpenaireErrorPageComponent} from './error/errorPage.component';
import {AdminLoginGuard} from "./openaireLibrary/login/adminLoginGuard.guard";
import {LoginGuard} from "./openaireLibrary/login/loginGuard.guard";
import {AdminDashboardGuard} from "./openaireLibrary/monitor-admin/utils/adminDashboard.guard";
import {HasDashboardGuard} from "./openaireLibrary/monitor/services/hasDashboard.guard";

const routes: Routes = [
  {
    path: 'reload',
    loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
    data: {hasSidebar: false}
  },
  {
    path: 'user-info',
    loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule),
    data: {hasSidebar: false}
  },
  {
    path: 'browse',
    loadChildren: () => import('./openaireLibrary/monitor//search-stakeholders/search-stakeholders.module').then(m => m.SearchStakeholdersModule),
    data: {hasSidebar: false, isFrontPage: true, isEmbeddedPage: true}
  },
  {
    path: 'error',
    component: OpenaireErrorPageComponent,
    data: {hasSidebar: false}
  },
  {
    path: 'admin',
    loadChildren: () => import('./openaireLibrary/monitor-admin/manageStakeholders/manageStakeholders.module').then(m => m.ManageStakeholdersModule),
    canActivateChild: [LoginGuard],
    data: {hasAdminMenu: true, hasSidebar: false}
  },
  {
    path: 'admin/user-info',
    loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule),
    data: {hasAdminMenu: true, hasSidebar: false}
  },
  {
    path: 'admin/admin-tools',
    loadChildren: () => import('./admin-tools/portal-admin-tools-routing.module').then(m => m.PortalAdminToolsRoutingModule),
    canActivateChild: [AdminLoginGuard],
    data: {hasAdminMenu: true, hasSidebar: false}
  },
  {
    path: 'admin/monitor/admin-tools',
    loadChildren: () => import('./admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
    canActivateChild: [AdminLoginGuard],
    data: {hasAdminMenu: true, hasSidebar: false, portal: 'monitor', monitorCurator: true, parentClass: 'monitor'}
  },
  {
    path: 'admin/:stakeholder',
    loadChildren: () => import('./admin-stakeholder/admin-stakeholder-routing.module').then(m => m.AdminStakeholderRoutingModule),
    canActivateChild: [AdminDashboardGuard],
    data: {hasAdminMenu: true, hasSidebar: false, activeMenuItem: "manage"}
  },
  {
    path: 'theme',
    loadChildren: () => import('./openaireLibrary/utils/theme/theme.module').then(m => m.ThemeModule),
    canActivateChild: [AdminLoginGuard],
    data: {hasSidebar: false, hasHeader: false, monitorCurator: true, monitorManager: true}
  },
  {
    path: '',
    loadChildren: () => import('./monitor/monitor.module').then(m => m.MonitorModule),
    canActivateChild: [HasDashboardGuard],
    data: {isFrontPage: true}
  },
  {
    path: '**',
    pathMatch: 'full',
    component: OpenaireErrorPageComponent,
    data: {hasSidebar: false}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
