import {ChangeDetectorRef, Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {FormBuilder} from "@angular/forms";
import {StakeholderInfo, Visibility} from "../openaireLibrary/monitor/entities/stakeholder";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {Session, User} from "../openaireLibrary/login/utils/helper.class";
import {StakeholderUtils} from "../openaireLibrary/monitor-admin/utils/indicator-utils";
import {Title} from "@angular/platform-browser";
import {BrowseStakeholderBaseComponent} from "../openaireLibrary/monitor/browse-stakeholder/browse-stakeholder-base.component";

@Component({
  selector: 'browse-stakeholder',
  templateUrl: 'browse-stakeholders.component.html',
  styleUrls: ['browse-stakeholders.component.less']
})
export class BrowseStakeholdersComponent extends BrowseStakeholderBaseComponent<StakeholderInfo> {
  user: User;
  stakeholderUtils: StakeholderUtils = new StakeholderUtils();

  visibilityIcon: Map<Visibility, string> = new Map<Visibility, string>([
    ["PUBLIC", 'earth'],
    ["PRIVATE", 'incognito'],
    ["RESTRICTED", 'restricted']
  ]);

  constructor(protected _route: ActivatedRoute,
              protected _router: Router,
              protected _title: Title,
              protected layoutService: LayoutService,
              protected cdr: ChangeDetectorRef,
              protected fb: FormBuilder,
              private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService) {
    super();
  }

  ngOnInit() {
    this.stakeholderType = this._route.snapshot.params.type;
    super.ngOnInit();
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      if (user) {
        this.user = user;
      }
    }));
    this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
      this.subscriptions.push(this._route.params.subscribe(params => {
        if(stakeholder?.umbrella) {
          this.stakeholderType = params['type'];
          this.reset();
          this.title = "Monitor Dashboard | " + stakeholder.name + " | " + "Browse " + this.entities[this.stakeholderType + 's'];
          this.description = "Monitor Dashboard | " + stakeholder.name + " | " + "Browse " + this.entities[this.stakeholderType + 's'];
          this.setMetadata();
          if (stakeholder.umbrella.children[this._route.snapshot.params['type']] == null) {
            this.navigateToError();
          }
          this.stakeholders = stakeholder.umbrella.children[this._route.snapshot.params['type']];
          this.stakeholders.forEach(stakeholder => {
            stakeholder.isManager = this.isManager(stakeholder);
            stakeholder.isMember = this.isMember(stakeholder)
          });
          this.filteredStakeholders = this.stakeholders;
          this.showLoading = false;
        }
      }));
    }));
  }

  reset() {
    this.sortBy = 'alphAsc';
    this.sortByChanged();
    this.sizeChanged(10);
    this.keywordControl.setValue(null);
    this.gridView = true;
  }

  private isManager(stakeholder) {
    return Session.isPortalAdministrator(this.user) || Session.isMonitorCurator(this.user)
        || Session.isCommunityCurator(this.user) || Session.isManager(stakeholder.type, stakeholder.alias, this.user);
  }

  private isMember(stakeholder) {
    return this.isManager(stakeholder) || Session.isSubscribedTo(stakeholder.type, stakeholder.alias, this.user);
  }

  hasPermission(item: StakeholderInfo) {
    return item.visibility === "PUBLIC" || (item.visibility === "RESTRICTED" && (item.isManager || item.isMember)) ||
        (item.visibility === "PRIVATE" && item.isManager);
  }
}
