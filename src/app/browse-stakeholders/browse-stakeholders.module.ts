import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowseStakeholdersComponent} from "./browse-stakeholders.component";
import {RouterModule, Routes} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {PagingModule} from "../openaireLibrary/utils/paging.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {Monitor} from "../monitor/monitor";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {earth, incognito, restricted} from "../openaireLibrary/utils/icons/icons";

let routes: Routes = new Monitor(':child').routes;
routes.splice(0, 0, {path: '', component: BrowseStakeholdersComponent, canDeactivate: [PreviousRouteRecorder], data: {hasSidebar: false}});

@NgModule({
  imports: [
    CommonModule, LoadingModule, SearchInputModule, InputModule, PagingModule, IconsModule, LogoUrlPipeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BrowseStakeholdersComponent],
  exports: [BrowseStakeholdersComponent]
})
export class BrowseStakeholdersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([earth, incognito, restricted]);
  }
}
