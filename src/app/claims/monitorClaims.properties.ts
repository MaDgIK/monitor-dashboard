import {ClaimsProperties} from "../openaireLibrary/claims/claim-utils/claims.properties";

export class MonitorClaimsProperties extends ClaimsProperties{
  constructor() {
    super();
    this.ALLOW_ORGANIZATION_LINKING = true;
    this.INLINE_ENTITY.show = false;
    this.INLINE_ENTITY.guideText = "";
    this.BASKET.target_title = "Link to";
    this.METADATA_PREVIEW.edit_target_icon = "west";
    this.METADATA_PREVIEW.edit_target_title = "Go to search for results"

  }
}
