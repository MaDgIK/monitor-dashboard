import {Component, OnInit} from '@angular/core';
import {properties} from "../../../environments/environment";
import {ActivatedRoute} from "@angular/router";
import {StakeholderService} from "../../openaireLibrary/monitor/services/stakeholder.service";
import {Title} from "@angular/platform-browser";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";

@Component({
  selector: 'openaire-claims-admin',
  template: `
    <div page-content>
      <div header>
        <ul class="uk-tab uk-margin-remove-bottom uk-margin-medium-top">
          <li><a routerLink="../../users/manager">Managers</a></li>
          <li ><a routerLink="../../users/member">Members</a></li>
          <li *ngIf="properties.environment != 'production'" class="uk-active"><a >Links</a></li>
        </ul>
      </div>
      <div inner>
        
    <claims-admin *ngIf="claimsInfoURL" [claimsInfoURL]="claimsInfoURL"  fetchBy="Organization"
                  [fetchId]="fetchId" [isConnect]="true">
    </claims-admin>
      </div>
    </div>
  `,
})
export class OpenaireClaimsAdminComponent implements OnInit {
  claimsInfoURL: string;
  userInfoURL: string;
  subscriptions = []
  fetchId = null;
  public properties: EnvProperties = properties;
  constructor( private route: ActivatedRoute, private stakeholderService: StakeholderService, private title: Title) {
  }
  
  public ngOnInit() {
    this.claimsInfoURL = properties.claimsInformationLink;
    if(this.route.snapshot.data.param === 'stakeholder') {
      this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
        if(stakeholder) {
          this.fetchId = stakeholder.index_id;
          this.title.setTitle(stakeholder.name + ' | Manage claims');
        }
      }));
    }
  }
}
