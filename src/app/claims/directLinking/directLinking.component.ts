import {Component, Input} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {StakeholderService} from "../../openaireLibrary/monitor/services/stakeholder.service";
import {Title} from "@angular/platform-browser";
import {MonitorClaimsProperties} from "../monitorClaims.properties";

@Component({
  selector: 'openaire-directLinking',
  template: `
    <div page-content>
      <div header>
        <!--        <users-tabs tab="claims"></users-tabs>-->
      </div>
      <div inner>
        <directLinking *ngIf="claimsProperties" [organizationClaim]="true" [communityId]="alias" [id]="id" [claimsProperties]="claimsProperties" ></directLinking>
      </div>
    </div>
  `
})
export class OpenaireDirectLinkingComponent {
  id;
  alias;
  subscriptions = [];
  claimsProperties;


  constructor(private route: ActivatedRoute, private stakeholderService: StakeholderService, private title: Title) {
    this.claimsProperties = new  MonitorClaimsProperties();
  }

  public ngOnInit() {
    if (this.route.snapshot.data.param === 'stakeholder') {
      this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
        if (stakeholder) {
          this.alias = stakeholder.alias;
          this.id = stakeholder.index_id;
          this.title.setTitle(stakeholder.name + ' | Claim Organization');
        }
      }));
    }
  }
}


