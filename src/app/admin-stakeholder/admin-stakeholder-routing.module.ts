import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      loadChildren: () => import('../openaireLibrary/monitor-admin/general/general.module').then(m => m.GeneralModule),
      pathMatch: 'full'
    },
    {
      path: 'users/links',
      loadChildren: () => import('../claims/claimsAdmin/claimsAdmin.module').then(m => m.LibClaimsAdminModule), pathMatch: 'full', data: {param: 'stakeholder', parentClass: 'monitor'}
    },
    {
      path: 'users',
      redirectTo: 'users/manager',
      pathMatch: 'full'
    },
    {
      path: 'users/:user_type',
      loadChildren: () => import('../users/users.module').then(m => m.UsersModule),
      pathMatch: 'full'
    },
    {
      matcher: HelperFunctions.routingMatcher(['indicators', 'indicators/:topic']),
      loadChildren: () => import('../openaireLibrary/monitor-admin/topic/topic.module').then(m => m.TopicModule),
      data: {hasInternalSidebar: true},
      pathMatch: 'full'
    },
    {
      path: 'umbrella',
      loadChildren: () => import('../openaireLibrary/monitor-admin/umbrella/umbrella.module').then(m => m.UmbrellaModule),
      pathMatch: 'full'
    },
    {
      path: 'admin-tools',
      loadChildren: () => import('../admin-tools/admin-tools-routing.module').then(m => m.AdminToolsRoutingModule),
      data: {param: 'stakeholder', parentClass: 'monitor'}
    },
    {
      path: 'claims/link',
     loadChildren: () => import('../claims/directLinking/directLinking.module').then(m => m.LibDirectLinkingModule),
      data: {param: 'stakeholder', parentClass: 'monitor'}
    }
  ])]
})
export class AdminStakeholderRoutingModule {

}
