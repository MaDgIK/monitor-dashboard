import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Role, Session, User} from './openaireLibrary/login/utils/helper.class';
import {UserManagementService} from "./openaireLibrary/services/user-management.service";
import {StakeholderService} from "./openaireLibrary/monitor/services/stakeholder.service";
import {Subscription} from "rxjs";
import {LayoutService} from "./openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {MenuItem} from "./openaireLibrary/sharedComponents/menu";
import {
  Category,
  Stakeholder,
  StakeholderType,
  Topic,
  Visibility
} from "./openaireLibrary/monitor/entities/stakeholder";
import {LinksResolver} from "./search/links-resolver";
import {Header} from "./openaireLibrary/sharedComponents/navigationBar.component";
import {ConfigurationService} from "./openaireLibrary/utils/configuration/configuration.service";
import {StakeholderUtils} from "./openaireLibrary/monitor-admin/utils/indicator-utils";
import {SmoothScroll} from "./openaireLibrary/utils/smooth-scroll";
import {ConnectHelper} from "./openaireLibrary/connect/connectHelper";
import {ResourcesService} from "./openaireLibrary/monitor/services/resources.service";
import {StringUtils} from "./openaireLibrary/utils/string-utils.class";
import {
  NotificationConfiguration
} from "./openaireLibrary/notifications/notifications-sidebar/notifications-sidebar.component";
import {SidebarBaseComponent} from "./openaireLibrary/dashboard/sharedComponents/sidebar/sidebar-base.component";
import {Breadcrumb} from './openaireLibrary/utils/breadcrumbs/breadcrumbs.component';
import {OpenaireEntities} from './openaireLibrary/utils/properties/searchFields';
import {HelperService} from "./openaireLibrary/utils/helper/helper.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.less']
})
export class AppComponent extends SidebarBaseComponent implements OnInit {
  user: User;
  updateStakeholder: boolean = true;
  hasSidebar: boolean = false;
  hasHeader: boolean = false;
  hasAdminMenu: boolean = false;
  hasInternalSidebar: boolean = false;
  breadcrumbs: Breadcrumb[] = [];
  isBrowse: boolean;
  isSearch: boolean;
  isFrontPage: boolean = false;
  isMobile: boolean = false;
  view: Visibility;
  menuItems: MenuItem[] = [];
  notificationGroupsInitialized: boolean = false;
  notificationConfiguration: NotificationConfiguration = new NotificationConfiguration();
  stakeholderUtils: StakeholderUtils = new StakeholderUtils();
  menuHeader: Header = {
    route: "/",
    url: null,
    title: "Default menu header",
    logoUrl: null,
    logoSmallUrl: null,
    position: 'left',
    badge: true,
    menuPosition: "center"
  };
  userMenuItems: MenuItem[] = [];
  adminMenuItems: MenuItem[] = [];
  stakeholder: Stakeholder = null;
  activeTopic: Topic = null;
  activeCategory: Category = null;
  loading: boolean = true;
  paramsResolved: boolean = false;
  innerWidth;
  projectUpdate: 'danger' | 'warning';
  paramsSubscription: Subscription;
  openaireEntities = OpenaireEntities;
  activeType: StakeholderType;
  divContents: any;
  public isEmbeddedPage: boolean;

  constructor(protected _route: ActivatedRoute,
              protected _router: Router,
              protected layoutService: LayoutService,
              protected cdr: ChangeDetectorRef,
              private userManagementService: UserManagementService,
              private smoothScroll: SmoothScroll,
              private stakeholderService: StakeholderService,
              private configurationService: ConfigurationService,
              private helper: HelperService,
              private resourcesService: ResourcesService) {
    super();
    this.initRouterParams(_route, event => {
      this.isBrowse = event.url.includes('browse');
      this.isSearch = event.url.includes('search/find');
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.getDivContents();
    if (typeof document !== 'undefined' && window) {
      this.innerWidth = window.innerWidth;
    }
    this.subscriptions.push(this.layoutService.hasHeader.subscribe(hasHeader => {
      this.hasHeader = hasHeader;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasAdminMenu.subscribe(hasAdminMenu => {
      this.hasAdminMenu = hasAdminMenu;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isFrontPage.subscribe(isFrontPage => {
      this.isFrontPage = isFrontPage;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isEmbeddedPage.subscribe(isEmbeddedPage => {
      this.isEmbeddedPage = isEmbeddedPage;
      this.cdr.detectChanges();
    }));
    this._route.queryParams.subscribe(params => {
      this.view = params['view'];
      if(this.stakeholder) {
        this.setSideBar();
      }
    });
    this.subscriptions.push(this.data.subscribe(data => {
      if (data && data.portal) {
        this.setProperties(data.portal);
      }
    }));
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.updateStakeholder = !this._router.url.includes('user-info');
      if (user) {
        this.user = user;
        if (!this.notificationGroupsInitialized) {
          this.setNotificationConfiguration();
        }
      } else if(this.user) {
        this.user = user;
        this.notificationGroupsInitialized = false;
        this.notificationConfiguration.availableGroups = [];
      }
      if(this.paramsSubscription) {
        this.paramsSubscription.unsubscribe();
      }
      this.paramsSubscription = this.params.subscribe(params => {
        if (this.paramsResolved) {
          this.loading = true;
          this.activeType = params['type'];
          if (params && params['stakeholder']) {
            let callback = (stakeholder: Stakeholder) => {
              if (stakeholder) {
                this.stakeholder = stakeholder;
                if (this.isChild) {
                  this.breadcrumbs = [
                    {name: this.stakeholder.parent.name, route: this.stakeholder.parent.alias}, 
                    {name: this.stakeholderUtils.entities[this.stakeholder.type + 's'], route: this.stakeholder.parent.alias + '/browse/' + this.stakeholder.type},
                    {name: this.stakeholder.name}
                  ];
                }
                this.updateStakeholder = false;
                this.buildMenu(this.stakeholder?.parent?this.stakeholder.parent:this.stakeholder);
                LinksResolver.setProperties(this.aliasPrefix + this.stakeholder.alias);
                this.setProperties(this.stakeholder.alias, this.stakeholder.type);
                this.setActives(params);
                this.setSideBar();
                this.loading = false;
              } else {
                this.stakeholder = null;
                LinksResolver.resetProperties();
                this.navigateToError();
                this.buildMenu();
                this.loading = false;
              }
            };
            if (params['child'] && (!this.stakeholder || this.stakeholder.alias !== params['child'] || this.updateStakeholder)) {
              this.subscriptions.push(this.stakeholderService.getChildStakeholder(params['stakeholder'], params['type'], params['child'], this.updateStakeholder).subscribe(callback));
            } else if (!params['child'] && (!this.stakeholder || this.stakeholder.alias !== params['stakeholder'] || this.updateStakeholder)) {
              this.subscriptions.push(this.stakeholderService.getStakeholder(params['stakeholder'], this.updateStakeholder).subscribe(callback));
            } else {
              this.buildMenu(this.stakeholder?.parent?this.stakeholder.parent:this.stakeholder);
              this.setActives(params);
              this.loading = false;
            }
          } else {
            LinksResolver.resetProperties();
            this.stakeholderService.setStakeholder(null);
            this.stakeholder = null;
            this.buildMenu();
            this.loading = false;
          }
        }
      });
      this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
        this.setProjectUpdate(stakeholder);
      }))
    }));
  }
  
  setActives(params: Params) {
    if (params && params['topic']) {
      this.activeTopic = this.stakeholder.topics.find(topic => topic.alias === decodeURIComponent(params['topic']) && this.hasPermission(topic.visibility));
    } else {
      this.activeTopic = this.stakeholder.topics.find(topic => this.hasPermission(topic.visibility));
    }
    if(this.activeTopic) {
      if (params && params['category']) {
        this.activeCategory = this.activeTopic.categories.find(category => category.alias === decodeURIComponent(params['category']) && this.hasPermission(category.visibility));
      } else {
        this.activeCategory = this.activeTopic.categories.find(category => this.hasPermission(category.visibility));
      }
    }
  }
  
  public setNotificationConfiguration() {
    this.notificationConfiguration.entities = this.stakeholderUtils.types.map(option => option.value);
    this.notificationConfiguration.service = 'monitor';
    this.notificationConfiguration.availableGroups = [];
    if (Session.isPortalAdministrator(this.user)) {
      this.notificationConfiguration.availableGroups.push({value: Role.PORTAL_ADMIN, label: 'Portal Administrators'});
    }
    for (let type of this.stakeholderUtils.types) {
      if (Session.isCurator(type.value, this.user) || Session.isPortalAdministrator(this.user)) {
        this.notificationConfiguration.availableGroups.push({value: Role.curator(type.value), label: type.label + ' Curators'});
        this.notificationConfiguration.availableGroups.push({value: Role.typeManager(type.value), label: type.label + ' Managers'});
        this.notificationConfiguration.availableGroups.push({value: Role.typeMember(type.value), label: type.label + ' Members'});
      }
    }
    this.subscriptions.push(this.stakeholderService.getMyStakeholders().subscribe(manageStakeholder => {
      let stakeholders = manageStakeholder.standalone;
      stakeholders.concat(manageStakeholder.umbrella);
      stakeholders.concat(manageStakeholder.dependent);
      stakeholders.forEach(stakeholder => {
        this.notificationConfiguration.availableGroups.push({
          value: Role.manager(stakeholder.type, stakeholder.alias),
          label: stakeholder.name + ' Managers'
        });
        this.notificationConfiguration.availableGroups.push({
          value: Role.member(stakeholder.type, stakeholder.alias),
          label: stakeholder.name + ' Members'
        });
      });
      this.notificationGroupsInitialized = true;
    }));
  }

  public setProjectUpdate(stakeholder: Stakeholder) {
    if(stakeholder?.projectUpdateDate && this.user) {
      let today = new Date();
      let date = new Date(stakeholder.projectUpdateDate);
      let months = (today.getFullYear() - date.getFullYear())*12 + (today.getMonth() - date.getMonth());
      if(months >= 12) {
        this.projectUpdate = 'danger';
      } else if (months >= 6 && months < 12) {
        this.projectUpdate = 'warning';
      } else {
        this.projectUpdate = null;
      }
    } else {
      this.projectUpdate = null;
    }
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    if(this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    this.userManagementService.clearSubscriptions();
    this.layoutService.clearSubscriptions();
    this.stakeholderService.clearSubscriptions();
    this.configurationService.clearSubscriptions();
    this.smoothScroll.clearSubscriptions();
  }

  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'monitor', '/').subscribe(contents => {
      this.divContents = contents;
    }));
  }

  private navigateToError() {
    this._router.navigate([this.properties.errorLink], {queryParams: {'page': this.properties.baseLink + this._router.url}});
  }
  
  public removeView() {
    this._router.navigate([], {relativeTo: this._route});
  }
  
  public login() {
    this.userManagementService.login();
  }
  
  get isHidden() {
    return this.stakeholder && !this.hasPermission(this.view?this.view:this.stakeholder.visibility);
  }
  
  get monitorLink() {
    if(this.properties.joomlaURL) {
      return this.properties.joomlaURL;
    }
    return "https://" + (this.properties.environment == 'beta' ? 'beta.' : '') + 'monitor.openaire.eu';
  }

  get isUmbrella(): boolean {
    return !!this.stakeholder?.umbrella && this.data.value.activeMenuItem === 'dashboard';
  }

  get isChild(): boolean {
    return !!this.stakeholder?.parent;
  }

  get aliasPrefix(): string {
    if(this.isChild) {
      return this.stakeholder.parent.alias + '/browse/' + this.stakeholder.type + '/';
    } else {
      return '/';
    }
  }
  
  private setSideBar() {
    let items: MenuItem[] = [];
    if (this.hasPermission(this.view?this.view:this.stakeholder.visibility)) {
      this.stakeholder.topics.forEach((topic: Topic) => {
        if (this.hasPermission(topic.visibility)) {
          let topicItem: MenuItem = new MenuItem(topic.alias, topic.name, "", this.aliasPrefix + this.stakeholder.alias + '/' + topic.alias,
            null, [], [], {}, {svg: topic.icon}, null, null, (
                  this.aliasPrefix + this.stakeholder.alias + '/' + topic.alias));
          topicItem.items = topic.categories.filter(category => this.hasPermission(category.visibility)).map(category => {
            return new MenuItem(category.alias, category.name, "", this.aliasPrefix + this.stakeholder.alias + '/' + topic.alias + '/' + category.alias,
              null, [], [], {}, {svg: topic.icon}, null, null,
                this.aliasPrefix + this.stakeholder.alias + '/' + topic.alias + '/' + category.alias);
          });
          items.push(topicItem);
        }
      });
      if (items.length === 0) {
        items.push(new MenuItem('noTopics', 'No topics available yet', "", "", false, [], [], {}));
      }
    }
    this.adminMenuItems = [];
    this.adminMenuItems.push(new MenuItem("general", "General", "", "/admin/" + this.stakeholder.alias, false, [], [], {}, {name: 'badge'}));
    this.adminMenuItems.push(new MenuItem("indicators", "Indicators", "", "/admin/" + this.stakeholder.alias + '/indicators', false, [], [], {}, {name: 'bar_chart'}, null, "uk-visible@m"));
    if (this.stakeholder.defaultId) {
      this.adminMenuItems.push(new MenuItem("users", "Users", "", "/admin/" + this.stakeholder.alias + "/users", false, [], [], {}, {name: 'group'}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/users"));
      if(this.stakeholder.umbrella) {
        this.adminMenuItems.push(new MenuItem("umbrella", "Umbrella", "", "/admin/" + this.stakeholder.alias + "/umbrella", false, [], [], {}, {name: 'workspaces'}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/umbrella"));
      }
      if (this.isCurator()) {
        this.adminMenuItems.push(new MenuItem("admin-tools", "Pages & Entities", "", "/admin/" + this.stakeholder.alias + "/admin-tools/pages", false, [], [], {}, {name: 'description'}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/admin-tools"));
      }
      if(this.stakeholder.type == "organization" && this.properties.environment != "production") {
        this.adminMenuItems.push(new MenuItem("claim", "Link results", "", "/admin/" + this.stakeholder.alias + "/claims/link", false, [], [], {}, {name: 'link', ratio: 1}, null, "uk-visible@m", "/admin/" + this.stakeholder.alias + "/claims/link"));
      }
    }
    this.backItem = new MenuItem("back", "Manage profiles", "", "/admin", false, [], null, {}, {name: 'west'});
    this.sideBarItems = items;
    this.hasSidebar = this.hasSidebar && this.sideBarItems.length > 0;
  }
  
  buildMenu(stakeholder: Stakeholder = null) {
    this.menuItems = [];
    this.userMenuItems = [];
    if (this.user) {
      if (this.isKindOfMonitorManager()) {
        this.userMenuItems.push(new MenuItem("", "Manage profiles",
          "", "/admin", false, [], [], {}));
      }
      if (Session.isPortalAdministrator(this.user)) {
        this.userMenuItems.push(new MenuItem("adminOptions", "Super Admin options", "", "/admin/admin-tools/portals", false, [], [], {},null, null, "uk-visible@m"));
      }
      if (this.isCurator()) {
        this.userMenuItems.push(new MenuItem("monitorOptions", "Monitor options", "", "/admin/monitor/admin-tools/pages", false, [], [], {},null, null, "uk-visible@m"));
      }
    }
    if (stakeholder) {
      this.userMenuItems.push(new MenuItem("", "User information", "", "/" + stakeholder.alias + "/user-info", false, [], [], {}));
      if (this.hasPermission((this.view && this.isManager(stakeholder))?this.view:stakeholder.visibility) && stakeholder.standalone) {
        this.menuItems.push(
          new MenuItem("dashboard", this.stakeholderUtils.entities.stakeholder,
            "", "/" + stakeholder.alias, false, [], null, {}
            , null, null, null, null)
        );
        this.menuItems.push(
          new MenuItem("search", "Browse Data", "", '/' + stakeholder.alias + '/' + LinksResolver.default.searchLinkToResults,
            false, [], null, null,
            null, null, null, null)
        );
        this.resourcesService.setResources(this.menuItems, '', this.monitorLink, '_blank');
        this.menuItems.push(new MenuItem("support", "Support", this.monitorLink + '/support/', "", false, [], null, {}));
        if (stakeholder.type === "funder") {
          this.menuItems.push(
            new MenuItem("develop", "Develop",
              "", "/" + stakeholder.alias + "/develop", false, [], null, {})
          );
        }
      }
      if (this.isManager(stakeholder)) {
        this.menuItems.push(
          new MenuItem("manage", "Manage",
            "", "/admin/" + stakeholder.alias, false, [], null, {}
            , null, null, "uk-visible@m", null)
        );
      }
      if (!this.hasAdminMenu && this.isFrontPage) {
        this.menuHeader = {
          route: './' + stakeholder.alias,
          url: null,
          title: stakeholder.name,
          logoUrl: StringUtils.getLogoUrl(stakeholder),
          logoSmallUrl: StringUtils.getLogoUrl(stakeholder),
          logoInfo: '<div class="uk-margin-left uk-width-medium"><div class="uk-margin-remove uk-text-background uk-text-bold uk-text-small">Monitor Dashboard</div>' +
            '<div class="uk-h6 uk-text-truncate uk-margin-remove">' + stakeholder.name + '</div></div>',
          position: 'left',
          badge: true,
          menuPosition: "center"
        };
      } else {
        this.menuHeader = {
          route: './' + stakeholder.alias,
          url: null,
          title: stakeholder.name,
          logoUrl: StringUtils.getLogoUrl(stakeholder),
          logoSmallUrl: StringUtils.getLogoUrl(stakeholder),
          logoInfo: '<div class="uk-margin-left uk-width-medium"><div class="uk-margin-remove uk-text-background uk-text-bold uk-text-small">Monitor Admin Dashboard</div>' +
            '<div class="uk-h6 uk-text-truncate uk-margin-remove">' + stakeholder.name + '</div></div>',
          position: 'left',
          badge: true,
          menuPosition: "center"
        };
      }
      if(stakeholder.type === 'publisher') {
        this.menuHeader.environmentBadge = {
          asset: 'assets/common-assets/prototype_flag.svg',
        }
      } else {
        this.menuHeader.environmentBadge = null;
      }
    } else {
      this.userMenuItems.push(new MenuItem("", "User information", null, '/user-info', false, [], [], {}, null, null, null, null, "_self"));
      this.menuHeader = {
        route: null,
        url: this.monitorLink,
        title: "Monitor",
        logoUrl: 'assets/common-assets/logo-services/monitor/main.svg',
        logoSmallUrl: "assets/common-assets/logo-services/monitor/small.svg",
        position: 'left',
        badge: true,
        menuPosition: "center"
      };
      if (this.hasAdminMenu) {
        this.adminMenuItems = [];
        this.backItem = null;
        this.adminMenuItems.push(new MenuItem("stakeholders", "Manage profiles", "", "/admin", false, [], [], {}, {name: 'settings'}));
        if (Session.isPortalAdministrator(this.user)) {
          this.adminMenuItems.push(new MenuItem("adminOptions", "Super Admin options", "", "/admin/admin-tools/portals", false, [], [], {}, {name: 'settings'}, null, "uk-visible@m", '/admin/admin-tools'));
        }
        if (Session.isPortalAdministrator(this.user) || Session.isMonitorCurator(this.user)) {
          this.adminMenuItems.push(new MenuItem("monitorOptions", "Monitor options", "", "/admin/monitor/admin-tools/pages", false, [], [], {}, {name: 'settings'}, null, "uk-visible@m", '/admin/monitor/admin-tools'));
        }
        this.hasAdminMenu = this.hasAdminMenu && this.adminMenuItems.length > 1;
      }
    }
  }
  
  public isCurator() {
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isMonitorCurator(this.user));
  }
  
  public isKindOfMonitorManager() {
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isMonitorCurator(this.user) || Session.isKindOfMonitorManager(this.user));
  }
  
  public isMember(stakeholder: Stakeholder) {
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isCurator(stakeholder.type, this.user)
      || Session.isManager(stakeholder.type, stakeholder.alias, this.user) || Session.isMember(stakeholder.type, stakeholder.alias, this.user));
  }
  
  public isManager(stakeholder: Stakeholder) {
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isCurator(stakeholder.type, this.user) || Session.isManager(stakeholder.type, stakeholder.alias, this.user));
  }
  
  public hasPermission(visibility: Visibility): boolean {
    if(visibility === 'PUBLIC') {
      return true;
    } else if(visibility === 'RESTRICTED') {
      return (!this.view || this.view === 'RESTRICTED') && this.isMember(this.stakeholder);
    } else {
      return !this.view && this.isManager(this.stakeholder);
    }
  }
  
  setProperties(id, type = null) {
    this.properties.adminToolsCommunity = id;
    if (type) {
      this.properties.adminToolsPortalType = type;
    } else {
      ConnectHelper.setPortalTypeFromPid(id);
    }
    this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
  }
}
