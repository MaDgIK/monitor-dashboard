import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'entities',
        loadChildren: () => import('../openaireLibrary/dashboard/entity/entities.module').then(m => m.EntitiesModule),
        pathMatch: 'full'
      },
      {
        path: 'classContents',
        loadChildren: () => import('../openaireLibrary/dashboard/divhelpcontent/class-help-contents.module').then(m => m.ClassHelpContentsModule),
        pathMatch: 'full'
      },
      {
        path: 'classContents/edit',
        loadChildren: () => import('../openaireLibrary/dashboard/divhelpcontent/class-help-content-form.module').then(m => m.ClassHelpContentFormModule),
        pathMatch: 'full'
      },
      {
        path: 'helptexts',
        loadChildren: () => import('../openaireLibrary/dashboard/helpTexts/page-help-contents.module').then(m => m.PageHelpContentsModule),
        pathMatch: 'full'
      },
      {
        path: 'helptexts/edit',
        loadChildren: () => import('../openaireLibrary/dashboard/helpTexts/page-help-content-form.module').then(m => m.PageHelpContentFormModule),
        pathMatch: 'full'
      },
      {
        path: 'pages',
        loadChildren: () => import('../openaireLibrary/dashboard/page/pages.module').then(m => m.PagesModule),
        pathMatch: 'full'
      }
    ])
  ]
})
export class AdminToolsRoutingModule {
}
