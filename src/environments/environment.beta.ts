import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonBeta} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  dashboard: 'monitor',
  adminToolsPortalType: "monitor",
  isDashboard: true,
  enablePiwikTrack: true,
  monitorStatsFrameUrl:"https://beta.services.openaire.eu/stats-tool/",
  useOldStatisticsSchema: true,
  adminToolsAPIURL: "https://beta.services.openaire.eu/uoa-monitor-service/",
  notificationsAPIURL: "https://beta.services.openaire.eu/uoa-monitor-service/notification/",
  adminToolsCommunity: "monitor",
  useHelpTexts:true,
  baseLink: "/dashboard",
  domain: "https://beta.monitor.openaire.eu",
  myClaimsLink: "../../users/links",
  joomlaURL: "https://monitor.openaire.eu"
};

export let properties: EnvProperties = {
  ...common, ...commonBeta, ...props
}
