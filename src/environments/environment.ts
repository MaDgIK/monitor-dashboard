// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  dashboard: 'monitor',
  adminToolsPortalType: "monitor",
  isDashboard: true,
  enablePiwikTrack: false,
  monitorStatsFrameUrl:"https://stats.madgik.di.uoa.gr/stats-api/",
  useOldStatisticsSchema: false,
  disableFrameLoad: true,
  adminToolsAPIURL: "http://dl170.madgik.di.uoa.gr:19380/uoa-monitor-service/",
  notificationsAPIURL: "http://dl170.madgik.di.uoa.gr:19380/uoa-monitor-service/notification/",
  adminToolsCommunity: "monitor",
  useHelpTexts: true,
  baseLink: "",
  domain: "http://mpagasas.di.uoa.gr:4600",
  myClaimsLink: "../../users/links",
  joomlaURL: "http://88.197.53.10"
};

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}
