import 'zone.js/node';

import {ngExpressEngine} from '@nguniversal/express-engine';
import * as express from 'express';
import * as compression from 'compression';
import {join} from 'path';

import {AppServerModule} from './src/main.server';
import {APP_BASE_HREF} from '@angular/common';
import {existsSync} from 'fs';
import {REQUEST, RESPONSE} from "./src/app/openaireLibrary/utils/tokens";
import {properties} from "./src/environments/environment";
import axios, {AxiosHeaders} from "axios";
import {Stakeholder} from "./src/app/openaireLibrary/monitor/entities/stakeholder";
import {CacheIndicators} from "./src/app/openaireLibrary/monitor-admin/utils/cache-indicators/cache-indicators";
import {Session, User} from "./src/app/openaireLibrary/login/utils/helper.class";
import {UserManagementService} from "./src/app/openaireLibrary/services/user-management.service";

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

// The Express app is exported so that it can be used by serverless Functions.
export function app() {
  const server = express();
  server.use(compression());
  const distFolder = join(process.cwd(), 'dist/monitor-dashboard/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';
  let cacheIndicators: CacheIndicators = new CacheIndicators();

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
    inlineCriticalCss: false
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(function (req, res, next) {
    let referer: string;
    if (req.headers.referer) {
      referer = Array.isArray(req.headers.referer) ? req.headers.referer[0] : (<string>req.headers.referer);
      referer = referer.split("?")[0];
    }
    if (referer && (referer.indexOf(".di.uoa.gr") != -1 || referer.indexOf(".openaire.eu") != -1 || referer.indexOf("88.197.53.10"))) {
      res.header('X-FRAME-OPTIONS', 'allow from ' + req.headers.referer);
      res.header('Access-Control-Allow-Origin', req.headers.origin);
      res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Cache-control, Expires, Content-Type, Pragma');
      res.header('Allow', 'GET, HEAD, OPTIONS');
    } else {
      res.header('X-FRAME-OPTIONS', 'SAMEORIGIN');
    }
    next();
  });

  server.use('/cache', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, DELETE');
    res.header('Access-Control-Max-Age', "1800");
    next();
  });

  server.post('/cache/:alias', jsonParser, async (req, res) => {
    await checkPermissions(req, res, (stakeholder, user) => {
      if (cacheIndicators.completed(stakeholder._id)) {
        res.send({
          id: stakeholder._id,
          report: cacheIndicators.createReport(stakeholder._id, cacheIndicators.stakeholderToCacheItems(stakeholder), stakeholder.name, user.email)
        });
      } else {
        res.status(409).send('There is another active caching process for this stakeholder');
      }
    });
  });

  server.get('/cache/:alias', async (req, res) => {
    await checkPermissions(req, res, stakeholder => {
      if (cacheIndicators.exists(stakeholder._id)) {
        res.send({
          id: stakeholder._id,
          report: cacheIndicators.getReport(stakeholder._id)
        });
      } else {
        res.status(404).send('There is not an active caching process for this stakeholder');
      }
    });
  });

  async function checkPermissions(req, res, access: (stakeholder, user) => void) {
    let headers: AxiosHeaders = new AxiosHeaders();
    headers.set('Cookie', req.headers.cookie);
    let userinfoRes = (await axios.get<any>(UserManagementService.userInfoUrl(), {
      withCredentials: true,
      headers: headers
    }).catch(error => {
      return error.response;
    }));
    if (userinfoRes.status === 200) {
      let user = new User(userinfoRes.data);
      let stakeholderRes = (await axios.get<Stakeholder>(properties.monitorServiceAPIURL + 'stakeholder/' + encodeURIComponent(req.params.alias), {
        withCredentials: true,
        headers: headers
      }).catch(error => {
        return error.response;
      }));
      if (stakeholderRes.status === 200) {
        let stakeholder = stakeholderRes.data;
        if (Session.isPortalAdministrator(user) || Session.isCurator(stakeholder.type, user)) {
          access(stakeholder, user);
        } else {
          res.status(403).send('You are forbidden to access this resource');
        }
      } else {
        res.status(stakeholderRes.status).send(stakeholderRes.statusText);
      }
    } else {
      res.status(userinfoRes.status).send(userinfoRes.data.message);
    }
  }

  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, {
        req, providers: [
          {
            provide: APP_BASE_HREF,
            useValue: req.baseUrl
          },
          {
            provide: REQUEST, useValue: (req)
          },
          {
            provide: RESPONSE, useValue: (res)
          }
        ]
      }
    );
  });

  return server;
}

function run() {
  const port = process.env.PORT || 4000;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
